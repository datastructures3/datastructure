package Tests;

import org.junit.Assert;
import org.junit.Test;
import recursion.powRecursion;

import static org.junit.Assert.*;

public class RecursionTest {

    @Test
    public void testReplaceX(){
        String output1 = powRecursion.replaceX("ccxckx");
        Assert.assertEquals("ccycky", output1);

        String output2 = powRecursion.replaceX("usxujx");
        Assert.assertEquals("usyujy", output2);
    }

    @Test
    public void testCountMy(){
        int result = powRecursion.countMy("myjshmyksjjsmy");
        Assert.assertEquals(3, result);
    }

    @Test
    public void testPow(){
        int result = powRecursion.pow(2,3);
        Assert.assertEquals(8, result);
    }

}