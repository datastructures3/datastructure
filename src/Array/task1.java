package Array;

import java.util.Scanner;

public class task1 {

    public static void main(String[] args) {
        Scanner myObj = new Scanner(System.in);

        try{
            System.out.println("Enter length of array");

            String arrayLength = myObj.nextLine();

            int arrayLengthInt = 0;

          //  try {
                 arrayLengthInt = Integer.parseInt(arrayLength);
         //   } catch (NumberFormatException e) {
               // System.out.println(arrayLength + " is not a number");
         //   }


            if(arrayLengthInt < 1 ){
                throw new IllegalArgumentException("Negative Number Detected, please enter your number again");
            }

            int[] myArray = new int[arrayLengthInt];

            for (int i = 0; i < myArray.length; i++){
                myArray[i]= (int) (Math.random() * (100 - 1 + 1) + 1);
            };

            for (int i = 0; i < myArray.length; i++){
                System.out.printf("%s%d", i==0 ? "" : ",", myArray[i]);
            };
            System.out.println( " ");


            for(int i=0; i< myArray.length; i++){
                boolean isPrime = true;

                for (int j=2; j < myArray[i]; j++){

                    if(myArray[i]%j==0){
                        isPrime = false;
                        break;
                    }
                }

                if(isPrime)

                    System.out.println(myArray[i] + " is the prime number in the array ");
            }

        }catch (IllegalArgumentException ex) {
            System.out.println("You must enter a number greater than 1 \n" + ex);
        }
    }
}
