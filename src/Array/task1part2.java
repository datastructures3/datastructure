package Array;

import java.util.ArrayList;
import java.util.Scanner;

public class task1part2 {

    public static void main(String[] args) {

        int [][] myArray;
        ArrayList<Double> list = new ArrayList<>();
        Scanner myLine = new Scanner(System.in);

        try {
            System.out.println("Enter length and width of array. It must be a positive number greater than 1");

            if(!myLine.hasNextInt()) {
                System.out.println("That's not a number!");
                myLine.next();
                return;
            }

            int row = myLine.nextInt();
            int col = myLine.nextInt();

            if(col < 1 || row < 1){
                throw new IllegalArgumentException("Negative Number Detected, please enter your number again");

            }

            myArray = new int[row][col];

            for (int i = 0; i < row; i++) {
                for (int j = 0; j < col; j++) {
                    myArray[i][j] = (int) (Math.random() * (99 - (-99) + 1) + (-99));
                }
            }

            for (int i = 0; i < row; i++) {
                for (int j = 0; j < col; j++) {
                    System.out.printf("%s%d", j==0 ? "" : ", ", myArray[i][j]);
                }
                System.out.println("");
            }
            System.out.println("-------");

           // 1) Sum of the array
            int sum = 0;
            for (int i = 0; i < row; i++) {

                for (int j = 0; j < col; j++) {
                    sum += myArray[i][j];
                }
            }
            System.out.println("Sum of all numbers in the array is: " + sum);
            System.out.println("-------");

            // 2) Sum of each of the row of the array
            for (int i = 0; i < row; i++) {
                int sumRow = 0;
                for (int j = 0; j < col; j++) {
                    sumRow += myArray[i][j];
                }
                System.out.println("Sum of the " + i  + " row in the array is: " + sumRow);
            }
            System.out.println("-------");

            // 3) sum of each of the column in the array
            for (int i = 0; i < row; i++) {
                int sumCol = 0;
                for (int j = 0; j < col; j++) {
                    sumCol += myArray[j][i];
                }
                System.out.println("Sum of the " + i + " column in the array is: " + sumCol);

            }
            System.out.println("-------");

            // 4) Standard deviation of all numbers in the array
            for (int i = 0; i < myArray.length; i++) {
                for (int j = 0; j < myArray[i].length; j++) {
                    list.add((double) myArray[i][j]);
                }
            }
            double SD = calculateSD(list);

            System.out.format("Standard Deviation = %.6f \n", SD);
            System.out.println("-------");


            // 5) Find pairs of numbers in the array whose sum is a prime
            System.out.println("Prime numbers are:");
            int sumPrime = 0;
            for (int i = 0; i < myArray.length; i++)
                for (int j = 0; j < myArray[i].length-1; j++){
                     sumPrime = myArray[i][j] + myArray[i][j+1];
                     if(isPrime(sumPrime)){
                         System.out.println(sumPrime + " is sum of " + myArray[i][j-1]  + " and " + myArray[i][j]);

                     }
                }


        } catch (IllegalArgumentException ex) {
            System.out.println("You must enter a positive number \n" + ex);
        }
    }
    public static double calculateSD(ArrayList<Double> list)
    {
        double sum = 0.0, standardDeviation = 0.0;
        int length = list.size();

        for(double num : list) {
            sum += num;
        }

        double mean = sum/length;

        for(double num: list) {
            standardDeviation += Math.pow(num - mean, 2);
        }

        return Math.sqrt(standardDeviation/length);
    }

    private static boolean isPrime(int number){

        for (int i=2; i <= number/2; i++){
            if (number % i == 0){
                return false;
            }
        }
        if (number < 2){
            return false;
        }
        return true;
    }

}
