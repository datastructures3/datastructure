package recursion;

public class powRecursion {

    public static void main(String[] args) {

        System.out.println("The power of 5 is " + pow(5,3));
        System.out.println("In the expression \"my\" appears " + countMy("bkjbhiamybkjskmybchimybhhib" + "times"));
        System.out.println("Lets use replace method: " + replaceX("xuxykkxk"));
    }

    public static int pow(int base, int m){
        if(m == 0) return 1;

        return base * pow(base, m - 1);
    }


    public static int countMy(String expr) {
        if(expr.length() < 2){
            return  0;
        }
        if (expr.substring(0,2).equals("my")){
            return 1 + countMy(expr.substring(2));
        }
        return  countMy(expr.substring(1));
    }

    public static String replaceX(String text){
        if(text.length() == 0){
            return text;
        }
        if (text.charAt(0) == 'x'){
            return 'y' + replaceX(text.substring(1));
        }
        return text.charAt(0) + replaceX(text.substring(1));
    }

    }

