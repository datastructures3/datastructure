package Task3CustomArray;

public class CustomArray {

    private  int[] data = new int[1];
    private  int[] dataTemp;

     public int size(){
        return data.length;
    }

    public void add(int value) {
        for (int i = 0; i < data.length; i++) {
            if (data[i] == 0) {
                data[i] = value;
                return;
            }
        }
        dataTemp = new int[data.length * 2];
        for (int j = 0; j < data.length; j++) {

            dataTemp[j] = data[j];
        }
        data = dataTemp;

        for (int i = 0; i < data.length; i++) {
            if (data[i] == 0) {
                data[i] = value;
                return;
            }
        }
    }

    public  void deleteByIndex(int index){
         dataTemp = new int[data.length];
        for (int j = 0; j < index; j++) {

            dataTemp[j] = data[j];
        }
        for (int j = index +1; j < data.length-1; j++) {

            dataTemp[j-1] = data[j];
        }
        data = dataTemp;
    }


    public boolean deleteByValue(int value){
        for (int i = 0; i < data.length; i++) {
            if (data[i] == value) {

                for (int j = i; j < data.length - 1; j++) {
                    data[j] = data[j+1];
                }  return true;
            }
        }
        return false;
    }

    public void insertValueAtIndex(int value, int index){

            if (data[index] == 0) {
                data[index] = value;return;
            }

            if (data[data.length-1] != 0){add(0);}


            if (data[index] != 0){
                dataTemp = new int[data.length];
                for (int j = 0; j < data.length; j++) {

                    dataTemp[j] = data[j];
                }}

                data[index] = value;


            for (int k = index+1; k < data.length; k++) {
                    data[k] = dataTemp[k-1];
                }
            return;

    }

        public void printArray ( int[] array){
            for (int i = 0; i < array.length; i++) {
                System.out.printf("%s%d", i == 0 ? "" : ",", array[i]);
            }

        }

        public void clear(){
        data = new int[1];
        }

        public int get(int index){
        int tempValue = 0;
        try {
             tempValue = data[index];


       }catch (IndexOutOfBoundsException ex){
            System.out.println("Current index does not exist\n" + ex);
        }return  tempValue;
        }

    public int[] getSlice(int startIdx, int length) {
        int[] foundArr = new int[length];
        try {
            for (int j = 0; j < length; j++) {

                foundArr[j] = data[startIdx + j];
            }

        }catch (IndexOutOfBoundsException ex){
            System.out.println("Current index does not exist\n" + ex);
        }return  foundArr;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("[");
        for(int i=0; i< data.length ; i++){
            builder.append(i==0 ? "": ", ");
            builder.append(data[i]);
        }
        builder.append("]");
        return builder.toString();
    }
}
