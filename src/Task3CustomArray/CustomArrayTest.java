package Task3CustomArray;

import org.junit.Assert;
import org.junit.Test;
import recursion.powRecursion;

import static org.junit.Assert.*;

public class CustomArrayTest {

    @Test
    public  void testAdd(){
        CustomArray ca = new CustomArray();
        ca.add(5);
        int output = ca.get(0);
        assertEquals(5, output);
    }

    @Test
    public void testDeleteByIndex(){
        CustomArray ca = new CustomArray();
        ca.add(1);
        ca.add(2);
        ca.add(3);
        ca.deleteByIndex(1);
        String output = "[1, 3, 0, 0]";
        assertEquals(output, ca.toString());
    }

    @Test
    public void testInsertValueAtIndex(){
        CustomArray ca = new CustomArray();
        ca.add(1);
        ca.add(2);
        ca.add(3);
        ca.add(5);
        ca.insertValueAtIndex(7,2);
        String output = "[1, 2, 7, 3, 5, 0, 0, 0]";
        assertEquals(output, ca.toString());
    }

}