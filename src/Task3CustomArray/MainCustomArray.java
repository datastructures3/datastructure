package Task3CustomArray;

public class MainCustomArray {
    public static void main(String[] args) {
        CustomArray ca = new CustomArray();

        ca.add(3);
        System.out.println();
        System.out.println(ca);
        System.out.println();
        System.out.println("The size of array is " + ca.size());
        ca.add(7);
        ca.add(8);
        System.out.println();
        System.out.println(ca);
        System.out.println();
        System.out.println("The size of array is " + ca.size());
        ca.add(9);
        ca.add(4);
        ca.add(2);
        ca.add(1);
        // add(9);
        System.out.println();
        System.out.println("The size of array is " + ca.size());

        System.out.println(ca);
        System.out.println();

        System.out.println("Lets delete 9");
        ca.deleteByValue(9);

        System.out.println(ca);

        System.out.println();
        System.out.println("Lets delete by index 4");
        ca.deleteByIndex(4);

        System.out.println(ca);


        System.out.println();
        System.out.println("Lets insert 2 with index 1");
        System.out.println();
        ca.insertValueAtIndex(2,1);

        System.out.println(ca);

        System.out.println();
        System.out.println("Lets insert 1 with index 6");
        System.out.println();
        ca.insertValueAtIndex(1,6);

        System.out.println(ca);

        System.out.println();
        ca.insertValueAtIndex(1,3);
        System.out.println();

        System.out.println(ca);

        System.out.println();
        System.out.println("the value with index 0 is " + ca.get(0));

        System.out.println();
        System.out.println("The size of array is " + ca.size());

        System.out.println();

        System.out.println("Sliced array " + java.util.Arrays.toString(ca.getSlice(3,3)));

        System.out.println();


        ca.clear();
        System.out.println();
        System.out.println("The size of array is " + ca.size());

    }
}
